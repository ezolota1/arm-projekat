const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");
const fs = require("fs");
const bcrypt = require("bcrypt")
const app = express();
const db = require("./baza");
const napuni=require("./napuniBazu")


db.sequelize.sync().then(() => napuni());


const Nastavnik = db.nastavnici;
const Predmet = db.predmeti;
const Student = db.studenti;
const Prisustvo = db.prisustva;

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(express.static(__dirname+"/public"));
app.use(session({
    secret: 'neka tajna sifra',
    resave: true,
    saveUninitialized: true
}));
 
app.get('/predmet',function(req,res){
    res.sendFile(__dirname+"/public/predmet.html");
});

app.get('/prisustvo',function(req,res){
    res.sendFile(__dirname+"/public/prisustvo.html");
});

app.get('/prijava',function(req,res){
    res.sendFile(__dirname+"/public/prijava.html");
});

app.get('/predmeti', function(req, res) {
    if(req.session.username=="null") {
        res.json({greska:"Nastavnik nije loginovan"});
    }
    else res.json(req.session);
});

var nastavnici=[];

app.post('/login', function(req, res) {
    let username, password="";
    let listaPredmeta=[];
    if(req.session.username!=null) {
        username=req.session.username;
        listaPredmeta=req.session.listaPredmeta;
        console.log("nastavnici1: ", nastavnici);

    } else {
        username=req.body['username'];
        password=req.body['password'];
        postoji= false;
        bcrypt.hash("PASSWORDHASH2", 10, function(err, hash) {
            console.log("Haaaaaash: ", hash);
        });


        nastavnici = Nastavnik.findAll({include: ["predmeti"]}).then((nastavnici) => {
            console.log("nastavnici2: ", nastavnici);

            nastavnici.forEach(nast => {
            
                const result=bcrypt.compareSync(password, nast.dataValues.password_hash);
                if(nast.dataValues.username==username && result) {
                    let predmeti=[];
                    nast.dataValues.predmeti.forEach(predm => {
                        predmeti.push(predm.dataValues.name);
                    });
                    req.session.username=username;
                    req.session.listaPredmeta=predmeti;
    
                    console.log("predmetiii: ", nast.dataValues.predmeti);
                    postoji=true;
                    console.log("UspjeEEH");

                    
                }
            });
            if(postoji) {
                console.log("Uspjesna prijava");
                res.json({"poruka":"Uspješna prijava"});
            } else {
                console.log("Neuspješna prijava");
                res.json({"poruka":"Neuspješna prijava"});
            }
        })
        //readCsv();   
        
    }

});

app.post('/logout', function(req, res) {
    req.session.username=null;
    req.session.listaPredmeta=null;
    res.sendFile(__dirname+"/public/prijava.html");
});


app.get("/predmet/:naziv", (req, res) => {
    console.log(req.params);
    let predmet=req.params.naziv;
    predmeti = Predmet.findAll({include: ["prisustva"]}).then((predmeti) => {
        console.log("predmeti: ", predmeti);

        predmeti.forEach(p => {
            if(p.dataValues.name==predmet) {
                let prisustva=[];
                let studenti=[];

                var bar = new Promise(async (resolve, reject) => {
                    let brojac=0;
                    for (const pris of p.dataValues.prisustva) {
                        brojac++;
                        console.log("Duzina: ", p.dataValues.prisustva.length)
                        await Student.findByPk(pris.dataValues.studentId).then((student) => {
                            let postojiStudent=false;
                            studenti.forEach(s => {
                                if(s.index==student.dataValues.index) {
                                    postojiStudent=true;
                                }
                            });
    
                            if(!postojiStudent) studenti.push({"ime":student.dataValues.ime, "index":student.dataValues.index});
    
                            prisustva.push({"sedmica":pris.dataValues.sedmica, "predavanja":pris.dataValues.predavanja, "vjezbe":pris.dataValues.vjezbe, "index":student.dataValues.index});
                            console.log("stuuuudenti: ", studenti);
                            console.log("PRISUUUSTVA: ", prisustva);
                            if(brojac==p.dataValues.prisustva.length) resolve();

    
                        }); 
                    }
                });
                
                bar.then(() => {
                    console.log("prisustva: ", prisustva);
                    console.log("studenti: ", studenti);
                    console.log("Uspjesna nadjen predmet");

                    res.json({"studenti":studenti, "prisustva":prisustva, "predmet":p.dataValues.name, "brojPredavanjaSedmicno":p.dataValues.brojPredavanjaSedmicno, "brojVjezbiSedmicno":p.dataValues.brojVjezbiSedmicno});                
                });

                
                
            }
        });
    })
    
});

app.post("/prisustvo/predmet/:naziv/student/:index", (req, res) => {
    console.log("ovdje je");
    let predmet=req.params.naziv;
    let indeks=req.params.index;

    let sedmica=req.body['sedmica'];
    let predavanja=req.body['predavanja'];
    let vjezbe=req.body['vjezbe'];
    console.log("To su te vjezbe: ", sedmica);


    predmeti = Predmet.findAll({include: ["prisustva"]}).then((predmeti) => {
        console.log("predmeti: ", predmeti);

        predmeti.forEach(p => {
            if(p.dataValues.name==predmet) {
                let prisustva=[];
                let studenti=[];

                var bar = new Promise(async (resolve, reject) => {
                    let brojac=0;
                    for (const pris of p.dataValues.prisustva) {
                        brojac++;
                        console.log("Duzina: ", p.dataValues.prisustva.length)
                        await Student.findByPk(pris.dataValues.studentId).then((student) => {
                            let postojiStudent=false;
                            studenti.forEach(s => {
                                if(s.index==student.dataValues.index) {
                                    postojiStudent=true;
                                }
                            });

                            if(student.dataValues.index==indeks && pris.dataValues.sedmica==sedmica) {
                                pris.dataValues.predavanja=predavanja;
                                pris.dataValues.vjezbe=vjezbe;
                                Prisustvo.update(
                                    {
                                        predavanja: predavanja,
                                        vjezbe: vjezbe
                                    },
                                    {
                                        where: { studentId: pris.dataValues.studentId }
                                    }
                                );
                            }
    
                            if(!postojiStudent) studenti.push({"ime":student.dataValues.ime, "index":student.dataValues.index});
    
                            prisustva.push({"sedmica":pris.dataValues.sedmica, "predavanja":pris.dataValues.predavanja, "vjezbe":pris.dataValues.vjezbe, "index":student.dataValues.index});
                            console.log("stuuuudenti: ", studenti);
                            console.log("PRISUUUSTVA: ", prisustva);
                            if(brojac==p.dataValues.prisustva.length) resolve();

    
                        }); 
                    }
                });
                
                bar.then(() => {
                    console.log("prisustva: ", prisustva);
                    console.log("studenti: ", studenti);
                    console.log("Uspjesna nadjen predmet");

                    res.json({"studenti":studenti, "prisustva":prisustva, "predmet":p.dataValues.name, "brojPredavanjaSedmicno":p.dataValues.brojPredavanjaSedmicno, "brojVjezbiSedmicno":p.dataValues.brojVjezbiSedmicno});                
                });

                
                
            }
        });
    })
    
});

app.listen(3000);
