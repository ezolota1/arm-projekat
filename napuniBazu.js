async function napuni() {
    const db = require("./baza");


const nast1=await db.nastavnici.create({
    username: "USERNAME",
    password_hash: "$2b$10$RYsiehWKc7Trd4iMgWIurujybUgeT.uxSGAHrqkJqFT2/s5F6DVba"
});

const nast2=await db.nastavnici.create({
    username: "USERNAME2",
    password_hash: "$2b$10$bb51PxNmt6WQXW7ws61H/OCNGPgDluV4QrQKwgMTaTy91g7kPvuMi"
});


const pred1= await db.predmeti.create({
    name: "PREDMET1",
    brojPredavanjaSedmicno: 2,
    brojVjezbiSedmicno: 2,
    nastavnikId: nast1.id
});

const pred2=await db.predmeti.create({
    name: "PREDMET2",
    brojPredavanjaSedmicno: 2,
    brojVjezbiSedmicno: 2,
    nastavnikId: nast1.id
});

const pred3=await db.predmeti.create({
    name: "PREDMET3",
    brojPredavanjaSedmicno: 2,
    brojVjezbiSedmicno: 2,
    nastavnikId: nast1.id
});

const pred4= await db.predmeti.create({
    name: "PREDMET4",
    brojPredavanjaSedmicno: 2,
    brojVjezbiSedmicno: 2,
    nastavnikId: nast2.id
});


const student1=await db.studenti.create({
    ime:"Neko Nekic",
    index: "12345"
});

const student2=await db.studenti.create({
    ime:"Drugi Neko",
    index: "12346"
});



db.prisustva.create({
    sedmica:1,
    predavanja:2,
    vjezbe:1,
    predmetId:pred1.id,
    studentId: student1.id
});

db.prisustva.create({
    sedmica:1,
    predavanja:2,
    vjezbe:2,
    predmetId:pred1.id,
    studentId:student2.id
});

db.prisustva.create({
    sedmica:2,
    predavanja:2,
    vjezbe:1,
    predmetId:pred1.id,
    studentId:student1.id
});

db.prisustva.create({
    sedmica:2,
    predavanja:2,
    vjezbe:0,
    predmetId:pred1.id,
    studentId:student2.id
});

db.prisustva.create({
    sedmica:1,
    predavanja:2,
    vjezbe:1,
    predmetId:pred2.id,
    studentId:student1.id
});

db.prisustva.create({
    sedmica:1,
    predavanja:2,
    vjezbe:2,
    predmetId:pred2.id,
    studentId:student2.id
});

db.prisustva.create({
    sedmica:2,
    predavanja:2,
    vjezbe:0,
    predmetId:pred2.id,
    studentId:student1.id
});

db.prisustva.create({
    sedmica:2,
    predavanja:2,
    vjezbe:2,
    predmetId:pred2.id,
    studentId:student2.id
});

db.prisustva.create({
    sedmica:1,
    predavanja:2,
    vjezbe:1,
    predmetId:pred3.id,
    studentId:student1.id
});

db.prisustva.create({
    sedmica:1,
    predavanja:2,
    vjezbe:2,
    predmetId:pred3.id,
    studentId:student2.id
});

db.prisustva.create({
    sedmica:2,
    predavanja:2,
    vjezbe:0,
    predmetId:pred3.id,
    studentId:student1.id
});

db.prisustva.create({
    sedmica:2,
    predavanja:2,
    vjezbe:2,
    predmetId:pred3.id,
    studentId:student2.id
});

db.prisustva.create({
    sedmica:1,
    predavanja:2,
    vjezbe:1,
    predmetId:pred4.id,
    studentId:student1.id
});

db.prisustva.create({
    sedmica:1,
    predavanja:2,
    vjezbe:2,
    predmetId:pred4.id,
    studentId:student2.id
});

db.prisustva.create({
    sedmica:2,
    predavanja:2,
    vjezbe:0,
    predmetId:pred4.id,
    studentId:student1.id
});

db.prisustva.create({
    sedmica:2,
    predavanja:2,
    vjezbe:2,
    predmetId:pred4.id,
    studentId:student2.id
});

}

module.exports=napuni;

//napuni();