const Sequelize = require("sequelize");
const sequelize = require("../baza");
 
module.exports = function (sequelize, DataTypes) {
    const Predmet = sequelize.define('predmet', {
       name: Sequelize.STRING,
       brojPredavanjaSedmicno: Sequelize.INTEGER,
       brojVjezbiSedmicno: Sequelize.INTEGER
    });
   return Predmet;
}

