const Sequelize = require("sequelize");
const sequelize = require("../baza");
 
module.exports = function (sequelize, DataTypes) {
    const Prisustvo = sequelize.define('prisustvo', {
       sedmica: Sequelize.INTEGER,
       predavanja: Sequelize.INTEGER,
       vjezbe: Sequelize.INTEGER
   });
   return Prisustvo;
}

