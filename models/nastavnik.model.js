const Sequelize = require("sequelize");
const sequelize = require("../baza");
 
module.exports = function (sequelize, DataTypes) {
    const Nastavnik = sequelize.define('nastavnik', {
       username: Sequelize.STRING,
       password_hash: Sequelize.STRING
   });
   return Nastavnik;
}

