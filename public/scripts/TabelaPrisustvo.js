let TabelaPrisustvo = function (divRef, podaci) {
    //privatni atributi modula
    //
    if (divRef.childNodes.length === 0) {
        console.log('✅ Element is empty');
    } else {
        while (divRef.firstChild) {
            divRef.removeChild(divRef.lastChild);
        }
    }

    
    var validni=true;
    for(let i=0; i<podaci.prisustva.length; i++) {
        if(podaci.prisustva[i].predavanja > podaci.brojPredavanjaSedmicno || podaci.prisustva[i].predavanja < 0 ||
            podaci.prisustva[i].vjezbe > podaci.brojVjezbiSedmicno) {
            validni=false;
            break;
        }
    }

    for(let i=0; i<podaci.studenti.length; i++) {
        let indeks=podaci.studenti[i].index;
        for(let j=i+1; j<podaci.studenti.length-1; j++) {
            if(indeks == podaci.studenti[j].index) {
                validni=false;
                break;
            }
        }
    }

    for(let i=0; i<podaci.prisustva.length; i++) {
        let indeks=podaci.prisustva[i].index;
        let validniIndeksi=false;
        for(let j=0; j<podaci.studenti.length; j++) {
            if(podaci.studenti[j].index == indeks) {
                validniIndeksi = true;
                break;
            }
        }
        if(!validniIndeksi) {
            validni=false;
            break;
        }
    }

    for(let i=0; i<podaci.prisustva.length; i++) {
        let indeks=podaci.prisustva[i].index;
        let sedmica=podaci.prisustva[i].sedmica;
        for(let j=i+1; j<podaci.prisustva.length-1; j++) {
            if(podaci.prisustva[j].index == indeks && podaci.prisustva[j].sedmica==sedmica) {
                validni = false;
                break;
            }
        }
    }

    var zadnjaSedmica=podaci.prisustva[podaci.prisustva.length-1].sedmica;
    var sedmice=[];
    for (let i=1; i<=zadnjaSedmica; i++) {
        sedmice.push(i);
    }

    for(let i=0; i<podaci.prisustva.length; i++) {
        let indexSedmice=sedmice.indexOf(podaci.prisustva[i].sedmica);
        if(indexSedmice!=-1) sedmice.splice(indexSedmice, 1);
    }

    if(sedmice.length!=0) validni=false;

    if(!validni) {
        const node = document.createElement("h1");
        const textnode = document.createTextNode("Podaci o prisustvu nisu validni!");
        node.appendChild(textnode);
        divRef.appendChild(node);
    } else {
        let predmet=podaci.predmet;
        console.log(predmet);
        const node = document.createElement("h1");
        const textnode = document.createTextNode(predmet);
        node.appendChild(textnode);
        divRef.appendChild(node);

        var brojKolona=podaci.prisustva[podaci.prisustva.length-1].sedmica;
        var brojRedova=podaci.studenti.length+1;

        if(brojKolona<15) brojKolona++;
        brojKolona+=2;

        var trenutnaSedmica=podaci.prisustva[podaci.prisustva.length-1].sedmica;

        pravljenjeTabele(trenutnaSedmica, brojKolona, brojRedova, divRef, podaci);
    }
    
    //implementacija metoda
    let sljedecaSedmica = function () {
        console.log("sljedeca");
        if(trenutnaSedmica<brojKolona-3) {
            var tabela_prisustvo = document.getElementById("tabela_prisustvo");
            tabela_prisustvo.parentNode.removeChild(tabela_prisustvo);
            var lijevi_button = document.getElementById("lijevi_button");
            lijevi_button.parentNode.removeChild(lijevi_button);
            var desni_button = document.getElementById("desni_button");
            desni_button.parentNode.removeChild(desni_button);
            trenutnaSedmica++;
            pravljenjeTabele(trenutnaSedmica, brojKolona, brojRedova, divRef, podaci);

            pravljenjeButtona(divRef, prethodnaSedmica, sljedecaSedmica);
    }
        
    }

    let prethodnaSedmica = function () {
        console.log("prethodna")
        if(trenutnaSedmica > 1) {
             var tabela_prisustvo = document.getElementById("tabela_prisustvo");
            tabela_prisustvo.parentNode.removeChild(tabela_prisustvo);
            var lijevi_button = document.getElementById("lijevi_button");
            lijevi_button.parentNode.removeChild(lijevi_button);
            var desni_button = document.getElementById("desni_button");
            desni_button.parentNode.removeChild(desni_button);
            trenutnaSedmica--;
            console.log(trenutnaSedmica);

            pravljenjeTabele(trenutnaSedmica, brojKolona, brojRedova, divRef, podaci);

            pravljenjeButtona(divRef, prethodnaSedmica, sljedecaSedmica);

        }
        
    }
    if(validni) pravljenjeButtona(divRef, prethodnaSedmica, sljedecaSedmica);
    
    return {
        sljedecaSedmica: sljedecaSedmica,
        prethodnaSedmica: prethodnaSedmica
    }
};

let pravljenjeTabele = function (trenutnaSedmica, brojKolona, brojRedova, divRef, podaci) {
    const tbl = document.createElement('table');

    tbl.setAttribute("id","tabela_prisustvo");

    for (let i = 0; i < brojRedova; i++) {
        const tr = tbl.insertRow();
        for (let j = 0; j < brojKolona; j++) {
            
                const td = tr.insertCell();
                
                if(i==0 && j==0) {
                    td.appendChild(document.createTextNode(`Ime i prezime`));
                    td.style.fontWeight = 'bold';
                }
                else if(i==0 && j==1) {
                    td.appendChild(document.createTextNode(`Index`));
                    td.style.fontWeight = 'bold';
                }
                else if(i==0 && j+1==brojKolona) {
                    td.appendChild(document.createTextNode(romanize(j-1)+"-"+romanize(15)));
                    td.style.fontWeight = 'bold';
                }
                else if(i==0) {
                    td.appendChild(document.createTextNode(romanize(j-1)));
                    td.style.fontWeight = 'bold';
                }
                else if(j==0 && i>0) {
                    td.appendChild(document.createTextNode(podaci.studenti[i-1].ime));
                }
                else if(j==1 && i>0) {
                    td.appendChild(document.createTextNode(podaci.studenti[i-1].index));
                }
                else if(j-1==trenutnaSedmica) {
                    const tblUnutarCelije = document.createElement('table');
                    td.setAttribute("id","celija_sa_tabelom");
                
                    tblUnutarCelije.setAttribute("id","tabela_unutar_celije");
                    for (let red = 0; red < 2; red++) { // redovi
                        const tr = tblUnutarCelije.insertRow();
                        for(let j=0; j < podaci.brojPredavanjaSedmicno; j++) {
                            const td = tr.insertCell();
                            if (red==0 && j<podaci.brojPredavanjaSedmicno) td.appendChild(document.createTextNode(`p${j+1}`));
                            else if(red>0) {
                                    for(let prisustvo=0; prisustvo<podaci.prisustva.length; prisustvo++) {
                                        if(podaci.prisustva[prisustvo].index==podaci.studenti[i-1].index && podaci.prisustva[prisustvo].sedmica==trenutnaSedmica) {
                                            if(j+1<=podaci.prisustva[prisustvo].predavanja) td.classList.add("prisutan");
                                            else td.classList.add("odsutan");
                                            td.addEventListener("click",function(){
                                                let pr=podaci.prisustva[prisustvo].predavanja;
                                                if (td.className=="prisutan") pr=pr-1;
                                                else pr=pr+1;
                                                PoziviAjax.postPrisustvo(podaci.predmet, podaci.prisustva[prisustvo].index, {sedmica:podaci.prisustva[prisustvo].sedmica,predavanja:pr,vjezbe:podaci.prisustva[prisustvo].vjezbe}, function(error, data) {
                                                    if(data!=null) TabelaPrisustvo(divRef, data);
                                                });
                                            });
                                        }
                                        
                                    }
                            }
                            
                        } 
                        
                        for(let j=0; j < podaci.brojVjezbiSedmicno; j++) {
                            const td = tr.insertCell();
                            if (red==0 && j<podaci.brojVjezbiSedmicno) td.appendChild(document.createTextNode(`v${j+1}`));
                            else if(red>0) {
                                
                                    for(let prisustvo=0; prisustvo<podaci.prisustva.length; prisustvo++) {
                                        if(podaci.prisustva[prisustvo].index==podaci.studenti[i-1].index && podaci.prisustva[prisustvo].sedmica==trenutnaSedmica) {
                                            if(j+1<=podaci.prisustva[prisustvo].vjezbe) td.classList.add("prisutan");
                                            else td.classList.add("odsutan");
                                            td.addEventListener("click",function(){
                                                let vj=podaci.prisustva[prisustvo].vjezbe;
                                                if (td.className=="prisutan") vj=vj-1;
                                                else vj=vj+1;
                                                console.log("Broj vjezbi: ", vj);
                                                PoziviAjax.postPrisustvo(podaci.predmet, podaci.prisustva[prisustvo].index, {sedmica:podaci.prisustva[prisustvo].sedmica,predavanja:podaci.prisustva[prisustvo].predavanja,vjezbe:vj}, function(error, data) {
                                                    if(data!=null) TabelaPrisustvo(divRef, data);
                                                });
                                            });
                                        }
                                        
                                    }
                                

                            } 
                        }
                    }
                        
                
                td.appendChild(tblUnutarCelije);

                }
                else {
                    var indeks=podaci.studenti[i-1].index;
                    var prisutnost=0;
                    for(let pom=j-2; pom<podaci.prisustva.length; pom++) {
                        if(podaci.prisustva[pom].sedmica==j-1 && podaci.prisustva[pom].index==indeks) {
                            prisutnost=podaci.prisustva[pom].predavanja+podaci.prisustva[pom].vjezbe;
                            td.appendChild(document.createTextNode(`${(prisutnost/(podaci.brojPredavanjaSedmicno + podaci.brojVjezbiSedmicno))*100}%`));
                            break;
                        } 
                    } 
                }
        }
    }

    divRef.appendChild(tbl);
}

let pravljenjeButtona = function(divRef, prethodnaSedmica, sljedecaSedmica) {
    var lijeviButton = document.createElement("BUTTON");
    var lijevaIkonica = document.createElement("i");
    lijevaIkonica.setAttribute("class", "fa-solid fa-arrow-left");
    lijeviButton.appendChild(lijevaIkonica);
    lijeviButton.onclick = prethodnaSedmica;
    divRef.appendChild(lijeviButton);
    lijeviButton.setAttribute("id","lijevi_button");

    var desniButton = document.createElement("BUTTON");
    var desnaIkonica = document.createElement("i");
    desnaIkonica.setAttribute("class", "fa-solid fa-arrow-right");
    desniButton.appendChild(desnaIkonica);
    desniButton.onclick = sljedecaSedmica;     
    divRef.appendChild(desniButton);
    desniButton.setAttribute("id","desni_button");
}

function romanize (num) {
    if (isNaN(num))
        return NaN;
    var digits = String(+num).split(""),
        key = ["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM",
               "","X","XX","XXX","XL","L","LX","LXX","LXXX","XC",
               "","I","II","III","IV","V","VI","VII","VIII","IX"],
        roman = "",
        i = 3;
    while (i--)
        roman = (key[+digits.pop() + (i * 10)] || "") + roman;
    return Array(+digits.join("") + 1).join("M") + roman;
}


