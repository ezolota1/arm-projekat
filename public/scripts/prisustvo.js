let div = document.getElementById("divSadrzaj");
var child1 = document.getElementById("naslov");
child1.parentNode.removeChild(child1);

var child2 = document.getElementById("odsjek");
child2.parentNode.removeChild(child2);

var child3 = document.getElementById("smjer");
child3.parentNode.removeChild(child3);

//instanciranje
let prisustvo = TabelaPrisustvo(div, {
	"studenti": [{
			"ime": "Neko Nekic",
			"index": 12345
		},
		{
			"ime": "Neko Nekić",
			"index": 12346
		},
		{
			"ime": "Neko Nekić",
			"index": 12347
		}
	],
	"prisustva": [{
			"sedmica": 1,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12345
		},
		{
			"sedmica": 1,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12346
		},
		{
			"sedmica": 1,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12347
		},
		{
			"sedmica": 2,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12345
		},
		{
			"sedmica": 2,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12346
		},
		{
			"sedmica": 2,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12347
		},
		{
			"sedmica": 3,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12345
		},
		{
			"sedmica": 3,
			"predavanja": 2,
			"vjezbe": 0,
			"index": 12346
		},
		{
			"sedmica": 3,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12347
		},
		{
			"sedmica": 4,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12345
		},
		{
			"sedmica": 4,
			"predavanja": 2,
			"vjezbe": 0,
			"index": 12346
		},
		{
			"sedmica": 4,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12347
		},
		{
			"sedmica": 5,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12345
		},
		{
			"sedmica": 5,
			"predavanja": 2,
			"vjezbe": 0,
			"index": 12346
		},
		{
			"sedmica": 5,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12347
		},
		{
			"sedmica": 6,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12345
		},
		{
			"sedmica": 6,
			"predavanja": 2,
			"vjezbe": 0,
			"index": 12346
		},
		{
			"sedmica": 6,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12347
		},
		{
			"sedmica": 7,
			"predavanja": 3,
			"vjezbe": 2,
			"index": 12345
		},
		{
			"sedmica": 7,
			"predavanja": 2,
			"vjezbe": 0,
			"index": 12346
		},
		{
			"sedmica": 7,
			"predavanja": 0,
			"vjezbe": 0,
			"index": 12347
		}
	],
	"predmet": "Razvoj mobilnih aplikacija",
	"brojPredavanjaSedmicno": 3,
	"brojVjezbiSedmicno": 2
}
);

//pozivanje metoda
//prisustvo.sljedecaSedmica();
//prisustvo.prethodnaSedmica();
