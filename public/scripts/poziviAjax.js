var ec2URL = window.location.protocol + '//'+ window.location.hostname;

const PoziviAjax = (()=>{

    //fnCallback u svim metodama se poziva kada stigne odgovor sa servera putem Ajax-a
    // svaki callback kao parametre ima error i data, error je null ako je status 200 i data je tijelo odgovora
    // ako postoji greška poruka se prosljeđuje u error parametar callback-a, a data je tada null
    function impl_getPredmet(naziv,fnCallback){
        var ajax=new XMLHttpRequest;
        let prisustva=[];
        console.log("naziv: ", naziv);
        ajax.open("GET", ec2URL+"/predmet/"+naziv, true);
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200) {
                var jsonRez = JSON.parse(ajax.responseText);
                prisustva=jsonRez;
                console.log("objekat: ",prisustva);
                fnCallback(null, prisustva);
            }
     
        }
        ajax.send();
    }

    // vraća listu predmeta za loginovanog nastavnika ili grešku da nastavnik nije loginovan
    function impl_getPredmeti(fnCallback){
        var ajax=new XMLHttpRequest;
        let predmeti=[];
        
        ajax.open("GET", ec2URL+"/predmeti", true);
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200) {
                var jsonRez = JSON.parse(ajax.responseText);
                console.log("hehe:", jsonRez.listaPredmeta);
                predmeti=jsonRez.listaPredmeta;
                fnCallback(null, predmeti);

            }
     
        }
        ajax.send();

        
    }
    
    function impl_postLogin(username,password,fnCallback){
        //fnCallback(null, {"poruka":"Uspješna prijava"});
        
        var ajax=new XMLHttpRequest;
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200) {
                var jsonRez = JSON.parse(ajax.responseText);
                console.log("Emina:", jsonRez.poruka);
                fnCallback(null,jsonRez);
            }
            else 
                fnCallback(ajax.statusText,null);
     
        }
        ajax.open("POST", ec2URL+"/login", true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify({username:username, password:password}));
    }

    function impl_postLogout(fnCallback){
        var ajax=new XMLHttpRequest;
        ajax.onreadystatechange = function() {// Anonimna funkcija
            fnCallback(null, null);
     
        }
        ajax.open("POST", ec2URL+'/logout', true);
        ajax.send();
    }
    //prisustvo ima oblik {sedmica:N,predavanja:P,vjezbe:V}
    function impl_postPrisustvo(naziv,index,prisustvo,fnCallback){
        console.log("ovdje: ", prisustvo);
        var ajax=new XMLHttpRequest;
        ajax.onreadystatechange = function() {// Anonimna funkcija
            if (ajax.readyState == 4 && ajax.status == 200) {
                var jsonRez = JSON.parse(ajax.responseText);
                console.log("Emina:", jsonRez);
                fnCallback(null,jsonRez);
            }
            else 
                fnCallback(ajax.statusText,null);
     
        }
        ajax.open("POST", ec2URL+'/predmet/'+naziv+'/student/'+index, true);
        ajax.setRequestHeader("Content-Type", "application/json");
        ajax.send(JSON.stringify({sedmica:prisustvo.sedmica, predavanja:prisustvo.predavanja, vjezbe:prisustvo.vjezbe}));
    }

    return{
        postLogin: impl_postLogin,
        postLogout: impl_postLogout,
        getPredmet: impl_getPredmet,
        getPredmeti: impl_getPredmeti,
        postPrisustvo: impl_postPrisustvo
    };
})();
