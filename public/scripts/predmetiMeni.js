var ec2URL = window.location.protocol + '//'+ window.location.hostname;

let predmeti = PoziviAjax.getPredmeti(function(error, data) {
    if(data==null) return;
    // A few variables for use later
    let divMeni=document.getElementById("meni");
var navElem = document.createElement("nav"),
navList = document.createElement("ul"), 
navItem, navLink;

navElem.setAttribute("class","topnav");

navElem.appendChild(navList);

// Cycle over each nav item
for (var i = 0; i < data.length; i++) {
    // Create a fresh list item, and anchor
    navItem = document.createElement("li");
    navLink = document.createElement("a");

    // Set properties on anchor
    navLink.href = "#";
    navLink.innerHTML = data[i];
    const naziv=data[i];
    navLink.addEventListener("click",function(){
        PoziviAjax.getPredmet(naziv, function(error, prisustva) {
            let div = document.getElementById("tabelaPrisustva");
            TabelaPrisustvo(div, prisustva);
        });
     });

    // Add anchor to list item, and list item to list
    navItem.appendChild(navLink);
    navList.appendChild(navItem);
}

    navItem = document.createElement("li");
    navLink = document.createElement("a");

    // Set properties on anchor
    navLink.href = "#";
    navLink.addEventListener("click",function(){
        PoziviAjax.postLogout(function(error, data) {
            
            window.location.assign(ec2URL+'/prijava.html');
            
        });
     });
    navLink.innerHTML = "Logout";

    navItem.appendChild(navLink);
    navList.appendChild(navItem);

// Set first list item as current
navList.children[0].className = "current";
divMeni.appendChild(navElem);

// Add list to body (or anywhere else)
/*
window.onload = function () {
    document.body.appendChild(navElem);
}
*/
});



