const Sequelize = require("sequelize");

var sequelize = new Sequelize('wt22',`${process.env.MYSQL_USER}`, `${process.env.MYSQL_PASSWORD}`, {
    host: `${process.env.MYSQL_DB_HOST}`,
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    port: "3306"
});


/*
const sequelize = new Sequelize("wt22", "root", "password", {
   host: "mysql-db", // "127.0.0.1",
   dialect: "mysql"
});
*/

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.nastavnici = require("./models/nastavnik.model")(sequelize, Sequelize);
db.predmeti = require("./models/predmet.model")(sequelize, Sequelize);

db.studenti = require("./models/student.model")(sequelize, Sequelize);
db.prisustva = require("./models/prisustvo.model")(sequelize, Sequelize);


db.nastavnici.hasMany(db.predmeti, { as: "predmeti" });
db.predmeti.belongsTo(db.nastavnici, {
  foreignKey: "nastavnikId",
  as: "nastavnik",
});

db.predmeti.hasMany(db.prisustva, { as: "prisustva" });
db.prisustva.belongsTo(db.predmeti, {
  foreignKey: "predmetId",
  as: "predmet",
});

db.studenti.hasMany(db.prisustva, { as: "prisustva" });
db.prisustva.belongsTo(db.studenti, {
  foreignKey: "studentId",
  as: "student",
});

module.exports = db;